package id.wahyurmd0512.pertemuan.kelima;

/**
 *
 * @author Wahyu Rmd
 */
public class PeluncuranRoketWhile {
    public static void main(String[] args) {
        int i;
        
        i = 100;
        while (i >= 0) {            
            System.out.println(i);
            i = i - 1;
        }
        System.out.println("Go");
    }
}
