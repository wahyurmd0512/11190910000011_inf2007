package id.wahyurmd0512.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class PenjumlahanDeretPecahanRepeat {
    public static void main(String[] args) {
        int x;
        float s;
        
        s = 0;            
        Scanner input = new Scanner(System.in);
        x = input.nextInt();
        
        do {
            s = s + (float) 1 / x;
            x = input.nextInt();
        } while (x != -1);
        System.out.println(s);
    }
}
