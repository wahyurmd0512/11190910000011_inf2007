package id.wahyurmd0512.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class HitungRerataWhile {
    public static void main(String[] args) {
        int i, x, jumlah;
        float rerata;
        
        jumlah = 0;
        i = 0;
        Scanner input = new Scanner(System.in);
        x = input.nextInt();
        
        while (x != -1) {            
            i = i + 1;
            jumlah = jumlah + x;
            x = input.nextInt();
        }
        
        if (i != 0) {
            rerata = jumlah / i;
            System.out.println(rerata);
        } else {
            System.out.println("Tidak ada nilai yang dimasukkan");
        }
    }
}
