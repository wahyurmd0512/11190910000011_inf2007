package id.wahyurmd0512.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class PerpangkatanFor {
    public static void main(String[] args) {
        int n, i;
        float a, p;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan nilai yg ingin dipangkatkan ");
        a = in.nextFloat();
        System.out.println("Masukkan pangkatnya ");
        n = in.nextInt();
        p = 1;
        for (i = 1; i <= n; i++) {
            p = p * a;
        }
        System.out.println("Hasilnya adalah " + p);
    }
}
