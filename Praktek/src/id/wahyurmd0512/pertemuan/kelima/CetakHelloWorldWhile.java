package id.wahyurmd0512.pertemuan.kelima;

/**
 *
 * @author Wahyu Rmd
 */
public class CetakHelloWorldWhile {
    public static void main(String[] args) {
        int i;
        
        i = 1;
        while (i <= 10) {            
            System.out.println("Hello, world");
            i = i + 1;
        }
    }
}
