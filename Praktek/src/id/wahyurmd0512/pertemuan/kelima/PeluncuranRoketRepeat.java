package id.wahyurmd0512.pertemuan.kelima;

/**
 *
 * @author Wahyu Rmd
 */
public class PeluncuranRoketRepeat {
    public static void main(String[] args) {
        int i;
        
        i = 100;
        
        do {            
            System.out.println(i);
            i = i - 1;
        } while (i >= 0);
        
        System.out.println("Go");
    }
 
}
