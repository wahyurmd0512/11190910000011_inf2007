package id.wahyurmd0512.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Wahyu Rmd
 */
public class PengurutanApung {
    public int[] getSortApung(int L[], int n) {
        int temp;
        for (int i = 0; i < n - 1; i++) {
            for (int k = n - 1; k > i; k--) {
                if (L[k - 1] < L[k]) {
                    temp = L[k - 1];
                    L[k - 1] = L[k];
                    L[k] = temp;
                }
            }
        }
        return L;
    }
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        PengurutanApung app = new PengurutanApung();
        System.out.print("Bilangan belum terurut: ");
        System.out.println(Arrays.toString(L));
        app.getSortApung(L, n);
        System.out.print("Bilangan yang sudah terurut: ");
        System.out.println(Arrays.toString(L));
    }
}
