package id.wahyurmd0512.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Wahyu Rmd
 */
public class ShellSort {
    public int [] getShellSort(int L[], int n){
        int step = n - 1;
        while (step > 1) {            
            step = (step / 3) + 1;
            for (int start = 0; start < step; start++) {
                int i = start + step;
                while (i <= n-1) {                    
                    int y = L[i];
                    int j = i - step;
                    boolean ketemu = false;
                    while ((j >= 0) && (!ketemu)) {                        
                        if (y < L[j]) {
                            L[j+step] = L[j];
                            j = j - step;
                        } else {
                            ketemu = true;
                        }
                    }
                    L[j+step] = y;
                    i = i + step;
                }
            }
        }
        return L;
    }
    
    public static void main(String[] args) {
        int L[] = {35, 17, 11, 28, 12, 41, 75, 15, 96, 58, 81, 94, 95};
        int n = L.length;
        ShellSort app = new ShellSort();
        System.out.print("Bilangan belum terurut: ");
        System.out.println(Arrays.toString(L));
        app.getShellSort(L, n);
        System.out.print("Bilangan yang sudah terurut: ");
        System.out.println(Arrays.toString(L));
    }
}
