package id.wahyurmd0512.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Wahyu Rmd
 */
public class SelectionSort {
    public int[] getSelectionSortMax(int L[], int n){
        for (int i = n - 1; i > 0; i--) {
            int imaks = 1;
            for (int j = 0; j <= i; j++) {
                System.out.println("i: " + i + ", j: " + j + " --> " + L[j]);
                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
            int temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }
    
    public int[] getSelectionSortMin(int L[], int n){
        for (int i = 0; i < n - 1; i++) {
            int imin = i;
            for (int j = i + 1; j < n; j++) {
                System.out.println("i: " + i + ", j: " + j + " --> " + L[j-1]);
                if (L[j] < L[imin]) {
                    imin = j;
                }
            }
            int temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }
    
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        SelectionSort app = new SelectionSort();
        System.out.print("Bilangan belum terurut: ");
        System.out.println(Arrays.toString(L));
        app.getSelectionSortMax(L, n);
        System.out.print("Bilangan yang sudah terurut dari nilai maks: ");
        System.out.println(Arrays.toString(L));
        app.getSelectionSortMin(L, n);
        System.out.print("Bilangan yang sudah terurut dari nilai min: ");
        System.out.println(Arrays.toString(L));
    }
}
