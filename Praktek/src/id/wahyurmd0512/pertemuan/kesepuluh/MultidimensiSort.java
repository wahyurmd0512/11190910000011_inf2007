package id.wahyurmd0512.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class MultidimensiSort {
    public int[][] getMultidimensiSort(int A[][], int nBar, int nKol){
        int temp, tambah = 0;
        int total = nBar*nKol;
        int Array[] = new int[total];
        
        for (int i = 0; i < nBar; i++) {
            for (int j = 0; j < nKol; j++) {
                Array[tambah] = A[i][j];
                tambah++;
            }
        }
        System.out.println("nilai-nilai larik penampung");
        System.out.println(Arrays.toString(Array));
        
        //bubble sort
        for (int i = 0; i <= total - 1; i++) {
            for (int j = total - 1; j > i; j--) {
                if (Array[j] < Array[j - 1]) {
                    temp = Array[j];
                    Array[j] = Array[j - 1];
                    Array[j - 1] = temp;
                }
            }
        }
        
        System.out.println("nilai larik penampung yang terurut");
        System.out.println(Arrays.toString(Array));
        
        tambah = 0;
        for (int i = 0; i < nBar; i++) {
            for (int j = 0; j < nKol; j++) {
                A[i][j] = Array[tambah];
                tambah++;
            }
        }
        return A;
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        MultidimensiSort mSort = new MultidimensiSort();
        int nBar, nKol;
        System.out.print("Masukkan jumlah baris: ");
        nBar = in.nextInt();
        System.out.print("Masukkan jumlah kolom: ");
        nKol = in.nextInt();
        int A[][] = new int[nBar][nKol];
        for (int i = 0; i < nBar; i++) {
            System.out.println("Masukkan nilai larik baris ke " + (i + 1));
            for (int j = 0; j < nKol; j++) {
                A[i][j] = in.nextInt();
            }
        }
        System.out.println("Larik sebelum terurut: ");
        System.out.println(Arrays.deepToString(A));
        mSort.getMultidimensiSort(A, nBar, nKol);
        System.out.println("Larik sesudah terurut: ");
        System.out.println(Arrays.deepToString(A));
    }
}
