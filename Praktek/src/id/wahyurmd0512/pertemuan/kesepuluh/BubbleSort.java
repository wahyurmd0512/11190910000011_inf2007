package id.wahyurmd0512.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Wahyu Rmd
 */
public class BubbleSort {
    public int[] getBubbleSort(int L[], int n) {
        int temp;
        for (int i = 0; i < n - 1; i++) {
            for (int k = n - 1; k > i; k--) {
                System.out.println("i: " + i + ", k: " + (k-1) + " --> " + L[k-1]);
                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k - 1] = temp;
                }
            }
        }
        return L;
    }
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        BubbleSort app = new BubbleSort();
        System.out.print("Bilangan belum terurut: ");
        System.out.println(Arrays.toString(L));
        app.getBubbleSort(L, n);
        System.out.print("Bilangan yang sudah terurut: ");
        System.out.println(Arrays.toString(L));
    }
}
