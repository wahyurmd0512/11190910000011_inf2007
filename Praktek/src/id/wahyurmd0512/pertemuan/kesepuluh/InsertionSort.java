package id.wahyurmd0512.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Wahyu Rmd
 */
public class InsertionSort {
    public int[] getInsertionSort(int L[], int n){
        for (int i = 2; i <= n - 1; i++) {
            int y = L[i];
            int j = i - 1;
            boolean ketemu = false;
            while ((j >= 0) && (!ketemu)) {                
                if (y < L[j]) {
                    L[j + 1] = L[j];
                    j = j - 1;
                } else {
                    ketemu = true;
                }
            }
            L[j + 1] = y;
        }
        return L;
    }
    
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        InsertionSort app = new InsertionSort();
        System.out.print("Bilangan belum terurut: ");
        System.out.println(Arrays.toString(L));
        app.getInsertionSort(L, n);
        System.out.print("Bilangan yang sudah terurut: ");
        System.out.println(Arrays.toString(L));
    }
}
