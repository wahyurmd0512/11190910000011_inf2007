package id.wahyurmd0512.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class Maksimum {
    private int A[], n;
    private int i, maks;
    
    public int Maks(int A[], int n){
        maks = -999;
        for (i = 1; i <= n; i++) {
            if (A[i] > maks) {
                maks = A[i];
            }
        }
        return maks;
    }
    
    public static void main(String[] args) {
        int A[] = new int[100];
        int n;
        
        Scanner in = new Scanner(System.in);
        Maksimum maksimum = new Maksimum();
        System.out.print("Masukkan Jumlah Larik: ");
        n = in.nextInt();
        for (int i = 1; i <= n; i++) {
            System.out.print("Masukkan bilangan : ");
            A[i] = in.nextInt();
        }
        System.out.println("Elemen Terbesar: " + maksimum.Maks(A, n));
    }
}
