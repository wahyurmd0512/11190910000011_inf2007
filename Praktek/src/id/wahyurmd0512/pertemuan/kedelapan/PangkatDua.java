package id.wahyurmd0512.pertemuan.kedelapan;

/**
 *
 * @author Wahyu Rmd
 */
public class PangkatDua {
    public static void main(String[] args) {
        int pangkat[] = new int[10];
        int i, k;
        
        k = 0;
        for (i = 0; i < 10; i++) {
            k = i + 1;
            pangkat[i] = k * k;
            System.out.println(pangkat[i]);
        }
    }
}
