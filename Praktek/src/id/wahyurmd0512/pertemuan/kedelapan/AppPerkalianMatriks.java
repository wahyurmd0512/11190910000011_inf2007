package id.wahyurmd0512.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AppPerkalianMatriks {
    public static void main(String[] args) {
        int nBar = 3, nKol = 5;
        Scanner in = new Scanner(System.in);
        int A[][] = new int[nBar][nKol];
        int hasil[][] = new int[nBar][nKol];
        
        System.out.println("Input Matriks A");
        for (int i = 0; i < nBar; i++) {
            for (int j = 0; j < nKol; j++) {
                System.out.print("Masukkan Matriks A [" + i + "][" + j + "] : ");
                A[i][j] = in.nextInt();
            }
        }
        
        PerkalianMatriks kali = new PerkalianMatriks();
        kali.Kali(A, nBar, nKol);
    }
}
