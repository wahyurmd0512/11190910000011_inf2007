package id.wahyurmd0512.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AplikasiMencariX {
    public static void main(String[] args) {
        int n, x;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan jumlah larik : ");
        n = in.nextInt();
        int A[] = new int[n];
        for (int i = 0; i < A.length; i++) {
            System.out.print("Masukkan larik [" + i + "] : ");
            A[i] = in.nextInt();
        }
        
        System.out.print("Masukkan angka yang di cari : ");
        x = in.nextInt();
        MencariX getX = new MencariX();
        getX.Cari(A, n, x);
    }
}
