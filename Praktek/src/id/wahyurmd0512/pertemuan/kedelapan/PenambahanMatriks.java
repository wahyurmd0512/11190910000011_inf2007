package id.wahyurmd0512.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class PenambahanMatriks {
    private int i, j;
    
    public int[][] getPenambahMatriks(int A[][], int B[][], int nBar, int nKol){
        int C[][] = new int[nBar][nKol];
        
        for (i = 0; i < nBar; i++) {
            for (j = 0; j < nKol; j++) {
                C[i][j] = A[i][j] + B[i][j];
                System.out.print("Hasil [" + i + "," + j + "] : ");
                System.out.println(C[i][j] + " ");
            }
        }
        
        return C;
    }
}
