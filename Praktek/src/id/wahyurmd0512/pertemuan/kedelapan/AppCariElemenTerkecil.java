package id.wahyurmd0512.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AppCariElemenTerkecil {
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan jumlah larik: ");
        int n = in.nextInt();
        int A[] = new int[n];
        for (int i = 0; i < A.length; i++) {
            System.out.print("Masukkan larik [" + i + "] : ");
            A[i] = in.nextInt();
        }
        CariElemenTerkecil minimum = new CariElemenTerkecil();
        System.out.println("Elemen Terkecil adalah: " + minimum.Min(A, n) );
    }
}
