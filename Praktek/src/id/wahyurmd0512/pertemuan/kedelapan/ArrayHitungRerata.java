package id.wahyurmd0512.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class ArrayHitungRerata {
    public static void main(String[] args) {
        int x[] = new int[6];
        float jumlah, u;
        
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < x.length; i++) {
            System.out.print("Masukkan nilai array [" + i + "] : ");
            x[i] = in.nextInt();
        }
        
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }
        
        jumlah = 0;
        for (int i = 0; i < x.length; i++) {
          jumlah = jumlah + x[i];
        }
        u = jumlah / 6;
        System.out.println("\nRata - rata : " + u);
    }
}
