package id.wahyurmd0512.pertemuan.kedelapan;

/**
 *
 * @author Wahyu Rmd
 */
public class CariElemenTerkecil {
    private int i, min;
    
    public int Min(int A[], int n){
        min = 999;
        for (i = 0; i < n; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        return min;
    }
}
