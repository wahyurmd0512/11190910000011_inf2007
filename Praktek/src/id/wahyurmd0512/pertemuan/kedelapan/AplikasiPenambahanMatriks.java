package id.wahyurmd0512.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AplikasiPenambahanMatriks {
    public static void main(String[] args) {
        int nBar = 0, nKol = 0;
        
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Baris : ");
        nBar = in.nextInt();
        System.out.print("Masukkan Kolom : ");
        nKol = in.nextInt();
        
        int A[][] = new int[nBar][nKol];
        int B[][] = new int[nBar][nKol];
        int C[][] = new int[nBar][nKol];

        System.out.println("Input Matriks A");
        for (int i = 0; i < nBar; i++) {
            for (int j = 0; j < nKol; j++) {
                System.out.print("Masukkan Matriks A [" + i + "," + j + "] : ");
                A[i][j] = in.nextInt();
            }
        }
        
        System.out.println("Input Matriks B");
        for (int i = 0; i < nBar; i++) {
            for (int j = 0; j < nKol; j++) {
                System.out.print("Masukkan Matriks B [" + i + "," + j + "] : ");
                B[i][j] = in.nextInt();
            }
        }
        
        PenambahanMatriks tambahMatriks = new PenambahanMatriks();
        tambahMatriks.getPenambahMatriks(A, B, nBar, nKol);

    }
}
