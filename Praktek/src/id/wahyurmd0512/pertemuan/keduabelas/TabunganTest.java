package id.wahyurmd0512.pertemuan.keduabelas;

/**
 *
 * @author Wahyu Rmd
 */
public class TabunganTest {
    public static void main(String[] args) {
        Tabungan t = new Tabungan(5000);
        System.out.println("Saldo awal: " + t.saldo);
        t.ambilUang(2300);
        System.out.println("Jumlah uang yang di ambil : 2300");
        System.out.println("Saldo sekarang: " + t.saldo);
    }
}
