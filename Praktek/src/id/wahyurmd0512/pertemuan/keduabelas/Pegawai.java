package id.wahyurmd0512.pertemuan.keduabelas;

/**
 *
 * @author Wahyu Rmd
 */
public class Pegawai {
    protected String nama;
    protected int gaji;
    
    public Pegawai(String nama, int gaji) {
        this.nama = nama;
        this.gaji = gaji;
    }
    
    public int infoGaji() {
        return this.gaji;
    }
}
