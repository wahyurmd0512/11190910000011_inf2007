package id.wahyurmd0512.pertemuan.kedua;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class Ratarata {

    public static void main(String[] args) {
        int x, y;
        float z;

        Scanner in = new Scanner(System.in);
        x = in.nextInt(); //suhu minimal
        y = in.nextInt(); //suhu maximal
        z = (x + y)/2;
        
        System.out.println("Suhu minimal suatu hari tertentu adalah " + x);
        System.out.println("Suhu maximal suatu hari tertentu adalah " + y);
        System.out.println("Suhu rata-rata dalam suatu hari tertentu adalah " + z);
    }
}
