package id.wahyurmd0512.pertemuan.kedua;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class Penjumlahan {

    public static void main(String[] args) {
        int a, b, c, d;

        Scanner in = new Scanner(System.in);
        a = in.nextInt();
        b = in.nextInt();
        c = in.nextInt();

        d = a + b + c;
        System.out.println("Hasil dari a + b + c = " + d );

    }
}
