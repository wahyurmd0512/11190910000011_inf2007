package id.wahyurmd0512.pertemuan.ketiga;

/**
 *
 * @author Wahyu Rmd
 */
public class BitwiseComplement {
    public static void main(String[] args) {
        int x = 8;
        System.out.println("x = " + x);
        int y = ~x;
        System.out.println("y = " + y);
    }
}
