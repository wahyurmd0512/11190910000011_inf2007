package id.wahyurmd0512.pertemuan.ketiga;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class KonversiJarak {
    public static void main(String[] args) {
        int jarakX, km, m, cm, sisaJarak, satuMeter = 100, satuKilometer = 100000;
        
        Scanner jarak = new Scanner(System.in);
        System.out.println("Masukkan jarak :");
        jarakX = jarak.nextInt();
        
        km = jarakX / satuKilometer;
        sisaJarak = jarakX % satuKilometer;
        m = sisaJarak / satuMeter;
        sisaJarak = sisaJarak % satuMeter;
        cm = sisaJarak;
        
        System.out.println("Jarak yang ditempuh semut adalah " + km + " Km " + m + " M " + cm + "Cm");
    }
}
