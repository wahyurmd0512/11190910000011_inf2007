package id.wahyurmd0512.pertemuan.ketiga;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class SelisihTanggal {

    public static void main(String[] args) {
        int tgl1, tgl2, tgl3, bulan1, bulan2, bulan3, tahun1, tahun2, tahun3, TotalHari1, TotalHari2, SelisihHari, Sisa;

        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Tanggal 1 ");
        tgl1 = in.nextInt();
        System.out.println("Masukkan Bulan 1 ");
        bulan1 = in.nextInt();
        System.out.println("Masukkan Tahun 1 ");
        tahun1 = in.nextInt();  
        System.out.println("Masukkan Tanggal 2 ");
        tgl2 = in.nextInt();
        System.out.println("Masukkan Bulan 2 ");
        bulan2 = in.nextInt();
        System.out.println("Masukkan Tahun 2 ");
        tahun2 = in.nextInt();
        System.out.println("Tanggal ke 1 = " + tgl1 + "-" + bulan1 + "-" + tahun1);
        System.out.println("Tanggal ke 2 = " + tgl2 + "-" + bulan2 + "-" + tahun2);
        
        TotalHari1 = (tahun1*365) + (bulan1*30) + tgl1;
        TotalHari2 = (tahun2*365) + (bulan2*30) + tgl2;
        SelisihHari = TotalHari2 - TotalHari1;
        System.out.println("Selisih Hari dari kedua tanggal tersebut adalah " + SelisihHari);

        tahun3 = SelisihHari / 365;
        Sisa = SelisihHari % 365;
        bulan3 = Sisa / 30;
        tgl3 = Sisa % 30;
        System.out.println(tahun3 + " Tahun " + bulan3 + " Bulan " + tgl3 + " Hari ");
    }
}
