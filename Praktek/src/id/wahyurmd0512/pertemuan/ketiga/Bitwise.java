package id.wahyurmd0512.pertemuan.ketiga;

/**
 *
 * @author Wahyu Rmd
 */
public class Bitwise {
    public static void main(String[] args) {
        int x = 5, y = 6;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("x & y = " + (x & y));
        System.out.println("x | y = " + (x | y));
        System.out.println("x ^ y = " + (x ^ y));
    }
}
