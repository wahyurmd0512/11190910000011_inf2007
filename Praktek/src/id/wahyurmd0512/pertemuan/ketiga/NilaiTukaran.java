package id.wahyurmd0512.pertemuan.ketiga;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class NilaiTukaran {
    public static void main(String[] args) {
        int pecahan1 = 1000, pecahan2 = 500, pecahan3 = 100, pecahan4 = 50, pecahan5 = 25, jmlPecahan1, jmlPecahan2, jmlPecahan3, jmlPecahan4, jmlPecahan5, nilaiUang, Sisa;
        
        Scanner uang = new Scanner(System.in);
        
        System.out.println("Masukkan nilai uang dalam kelipatan 25 :");
        nilaiUang = uang.nextInt();
        jmlPecahan1 = nilaiUang / pecahan1;
        Sisa = nilaiUang % pecahan1;
        jmlPecahan2 = Sisa / pecahan2;
        Sisa = nilaiUang % pecahan2;
        jmlPecahan3 = Sisa / pecahan3;
        Sisa = nilaiUang % pecahan3;
        jmlPecahan4 = Sisa / pecahan4;
        Sisa = nilaiUang % pecahan4;
        jmlPecahan5 = Sisa / pecahan5;
        
        System.out.println("Jumlah Pecahan Rp 1000 ada " + jmlPecahan1 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 500 ada " + jmlPecahan2 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 100 ada " + jmlPecahan3 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 50 ada " + jmlPecahan4 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 25 ada " + jmlPecahan5 + " pecahan");
        
    }
}
