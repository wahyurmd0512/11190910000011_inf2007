package id.wahyurmd0512.pertemuan.kesebelas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class TulisArsipBilanganBulat {

    public static void main(String[] args) {
        String Bil = "d:\\temp\\tulis.txt";
        int n, i;
        Scanner sc = new Scanner(System.in);
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(Bil));
            System.out.print("N = ");
            n = sc.nextInt();
            for (i = 0; i <= n; i++) {
                pw.println(i);
            }
            pw.close();
        } catch (IOException e) {
            System.err.println("Error " + e.getMessage());
        }
    }
}
