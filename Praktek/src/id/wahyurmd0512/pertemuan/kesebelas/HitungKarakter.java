package id.wahyurmd0512.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wahyu Rmd
 */
public class HitungKarakter {
    public int getHitungKarakter(FileReader T){
        int n = 0;
        char c[];
        Scanner line = new Scanner(new BufferedReader(T));
        while (line.hasNext()) {            
            c = line.next().toCharArray();
            for (char d : c) {
                if (d == 'a'){
                    n = n + 1;
                }
            }
        }
        return n;
    }
    
    public static void main(String[] args) {
        HitungKarakter baca = new HitungKarakter();
        int a;
        try {
            a = baca.getHitungKarakter(new FileReader("d:\\temp\\T.txt"));
            System.out.println(a);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HitungKarakter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
