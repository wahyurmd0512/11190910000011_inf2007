package id.wahyurmd0512.pertemuan.kesebelas;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Wahyu Rmd
 */
public class Mahasiswa implements Serializable {

    private final static long serialVersionUID = 1L;
    private String nama;
    private String NIM;
    private String kodeMK;
    private int SKS;
    private char nilai;

    public Mahasiswa() {
       
    }
    
    public Mahasiswa(String NIM, String nama, String kodeMK, int SKS, char nilai) {
        this.nama = nama;
        this.NIM = NIM;
        this.kodeMK = kodeMK;
        this.SKS = SKS;
        this.nilai = nilai;
    }
    
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNIM() {
        return NIM;
    }

    public void setNIM(String NIM) {
        this.NIM = NIM;
    }

    public String getKodeMK() {
        return kodeMK;
    }

    public void setKodeMK(String kodeMK) {
        this.kodeMK = kodeMK;
    }

    public int getSKS() {
        return SKS;
    }

    public void setSKS(int SKS) {
        this.SKS = SKS;
    }

    public char getNilai() {
        return nilai;
    }

    public void setNilai(char nilai) {
        this.nilai = nilai;
    }

    @Override
    public String toString() {
        return "Mahasiswa{" + "nama=" + nama + ", nim=" + NIM + ", kodemk=" + kodeMK + ", sks=" + SKS + ", nilai=" + nilai + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.nama);
        hash = 19 * hash + Objects.hashCode(this.NIM);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mahasiswa other = (Mahasiswa) obj;
        if (!Objects.equals(this.nama, other.nama)) {
            return false;
        }
        if (this.NIM != other.NIM) {
            return false;
        }
        return true;
    }

}
