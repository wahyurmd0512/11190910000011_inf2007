package id.wahyurmd0512.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wahyu Rmd
 */
public class KataTerpanjang {
    public String getCariKata(FileReader K) {
        String L[] = new String[100];
        String kata = null;
        Scanner in = new Scanner(new BufferedReader(K));
        int n = 0;
        while (in.hasNext()) {            
            L[n] = in.next();
            n++;
        }
        for (int i = 0; i < n - 1; i++) {
            if (L[i].length() < L[i + 1].length()) {
                kata = L[i + 1];
            }
        }
        return kata;
    }
    
    public static void main(String[] args) {
        KataTerpanjang kt = new KataTerpanjang();
        String a;
        try {
            a = kt.getCariKata(new FileReader("d:\\temp\\kata terpanjang.txt"));
            System.out.println("Kata terpanjang: " + a);
        } catch (FileNotFoundException e) {
            Logger.getLogger(KataTerpanjang.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
