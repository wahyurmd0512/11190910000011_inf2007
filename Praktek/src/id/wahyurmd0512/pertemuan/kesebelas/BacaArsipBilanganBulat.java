package id.wahyurmd0512.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class BacaArsipBilanganBulat {
    public static void main(String[] args) {
        String Bil = "d:\\temp\\tulis.txt";
        Scanner line;
        int x;
        try {
            BufferedReader inFile = new BufferedReader(new FileReader(Bil));
            line = new Scanner(inFile);
            while (line.hasNextInt()) {
                x = line.nextInt();
                System.out.println(x);
            }
            inFile.close();
        } catch (IOException e) {
            System.err.println("Error I/O : " + e.getMessage());
        }
        
    }
}
