package id.wahyurmd0512.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wahyu Rmd
 */
public class HitungKarakterAn {

    public boolean getCariKarakter(FileReader B) {
        boolean ketemu = false;
        char L[];
        Scanner line = new Scanner(new BufferedReader(B));
        while (line.hasNext()) {            
            L = line.next().toCharArray();
            for (int i = 0; i < L.length-1; i++) {
                if (L[i] == 'a') {
                    if (L[i + 1] == 'n') {
                        ketemu = true;
                    }
                }
            }
        }
        return ketemu;
    }
    
    public static void main(String[] args) {
        HitungKarakterAn baca = new HitungKarakterAn();
        boolean a;
        try {
            a = baca.getCariKarakter(new FileReader("d:\\temp\\HK.txt"));
            System.out.println("Ada kata an = " + a);
        } catch (FileNotFoundException e) {
            Logger.getLogger(HitungKarakterAn.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
