package id.wahyurmd0512.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AplikasiGenap {
    public static void main(String[] args) {
        int bil;
        Genap genap = new Genap();
        
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan bilangan : ");
        bil = in.nextInt();
        if (genap.getHasil(bil)) {
            System.out.println(bil + " adalah bilangan genap");
        } else {
            System.out.println(bil + " bukan bilangan genap");
        }
    }
}
