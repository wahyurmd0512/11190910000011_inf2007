package id.wahyurmd0512.pertemuan.ketujuh;

/**
 *
 * @author Wahyu Rmd
 */
public class Bulan {
    private int noBulan;
    public String getBulan(int noBulan) {
        String nama = null;

        switch (noBulan) {
            case 1:
                nama = "Januari";
                break;
            case 2:
                nama = "Februari";
                break;
            case 3:
                nama = "Maret";
                break;
            case 4:
                nama = "April";
                break;
            case 5:
                nama = "Mei";
                break;
            case 6:
                nama = "Juni";
                break;
            case 7:
                nama = "Juli";
                break;
            case 8:
                nama = "Agustus";
                break;
            case 9:
                nama = "September";
                break;
            case 10:
                nama = "Oktober";
                break;
            case 11:
                nama = "November";
                break;
            case 12:
                nama = "Desember";
                break;
            default:
                System.out.println("Nomor bulan tidak sesuai");
                break;
        }
        return nama;
    }
}
