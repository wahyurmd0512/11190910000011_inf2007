package id.wahyurmd0512.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AplikasiBulan {
    public static void main(String[] args) {
        int noBul;
        Bulan namaBulan = new Bulan();
        
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan no bulan : ");
        noBul = in.nextInt();
        
        System.out.println(namaBulan.getBulan(noBul));
    }
}
