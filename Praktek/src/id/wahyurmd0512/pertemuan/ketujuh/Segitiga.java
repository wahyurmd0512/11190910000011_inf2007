package id.wahyurmd0512.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class Segitiga {
    private float alas, tinggi, luas;
    
    public Segitiga(){
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan alas : ");
        alas = in.nextFloat();
        System.out.println("Masukkan tinggi : ");
        tinggi = in.nextFloat();
        
        luas = (alas * tinggi) / 2;
        
        System.out.println("Luasnya : " + luas);
    }
    
}
