package id.wahyurmd0512.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AplikasiPersegiPanjang {
    public static void main(String[] args) {
        // mencoba kosnsstruktor default
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();
        
        Scanner in = new Scanner(System.in);
        double panjang, lebar;
        
        System.out.println("Masukkan Panjang: ");
        panjang = in.nextDouble();
        
        System.out.println("Masukkan Lebar: ");
        lebar = in.nextDouble();
        
        PersegiPanjang persegiPanjang = new PersegiPanjang(panjang, lebar);
        persegiPanjang.getInfo();
        System.out.println("Luas : " + persegiPanjang.getLuas());
        System.out.println("Keliling : " + persegiPanjang.getKeliling());
    }
}
