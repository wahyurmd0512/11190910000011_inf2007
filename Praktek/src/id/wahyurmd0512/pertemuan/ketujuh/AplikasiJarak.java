package id.wahyurmd0512.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AplikasiJarak {
    public static void main(String[] args) {
        int x1, x2, y1, y2;
        Scanner in = new Scanner(System.in);
        System.out.print("Nilai x1 = ");
        x1 = in.nextInt();
        System.out.print("Nilai x2 = ");
        x2 = in.nextInt();
        System.out.print("Nilai y1 = ");
        y1 = in.nextInt();
        System.out.print("Nilai y2 = ");
        y2 = in.nextInt();
        
        Jarak hitungJarak = new Jarak();
        System.out.println("Jarak titik 1 dengan titik 2 : " + hitungJarak.Jarak(x1, x2, y1, y2));
    }
}
