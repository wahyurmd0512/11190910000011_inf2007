package id.wahyurmd0512.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class AplikasiKabisat {
    public static void main(String[] args) {
        int tahun;
        Kabisat kabisat = new Kabisat();
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan tahun kabisat : ");
        tahun = in.nextInt();
        if (kabisat.getHasil(tahun)) {
            System.out.println("Tahun Kabisat");
        } else {
            System.out.println("Bukan Tahun Kabisat");
        }
    }
}
