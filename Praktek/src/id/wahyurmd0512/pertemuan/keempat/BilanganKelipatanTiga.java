package id.wahyurmd0512.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class BilanganKelipatanTiga {
    public static void main(String[] args) {
        int bilBul;

        Scanner bilangan = new Scanner(System.in);
        System.out.println("Masukkan Nilai Bilangan Bulat Positif:");
        bilBul = bilangan.nextInt();

        if (bilBul % 3 == 0) {
            System.out.println("Kelipatan 3");
        }
    }
}
