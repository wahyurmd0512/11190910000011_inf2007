package id.wahyurmd0512.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class MenuPerhitungan {

    public static void main(String[] args) {
        int noMenu;
        float panjang, lebar, luas, keliling, diagonal;

        Scanner in = new Scanner(System.in);
        System.out.println("Panjang : ");
        panjang = in.nextFloat();
        System.out.println("Lebar : ");
        lebar = in.nextFloat();
        System.out.println("Menu Empat Persegi Panjang");
        System.out.println("1. Hitung Luas \n2. Hitung Keliling \n3. Hitung Panjang Diagonal \n4. Keluar Program");
        System.out.println("Masukkan pilihan anda (1/2/3/4) ?");
        noMenu = in.nextInt();

        switch (noMenu) {
            case 1:
                System.out.println("Panjang = " + panjang + " , Lebar = " + lebar);
                luas = panjang * lebar;
                System.out.println("Luasnya adalah " + luas);
                break;
            case 2:
                System.out.println("Panjang = " + panjang + " , Lebar = " + lebar);
                keliling = (2 * panjang) + (2 * lebar);
                System.out.println("Kelilingnya adalah " + keliling);
                break;
            case 3:
                System.out.println("Panjang = " + panjang + " , Lebar = " + lebar);
                diagonal = (panjang * panjang) + (lebar * lebar);
                System.out.println("Diagonalnya adalah " + diagonal);
                break;
            case 4:
                System.out.println("Keluar program... sampai jumpa");
                break;
        }
    }
}
