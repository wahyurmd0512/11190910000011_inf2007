package id.wahyurmd0512.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class GanjilGenap {
    public static void main(String[] args) {
        int bilangan;
        
        Scanner bil = new Scanner(System.in);
        System.out.println("Masukkan angka : ");
        bilangan = bil.nextInt();
        
        if (bilangan % 2 == 0) {
            System.out.println("angka " + bilangan + " adalah bilangan genap");
        } else {
            System.out.println("angka " + bilangan + " adalah bilangan ganjil");
        }
    }
}
