package id.wahyurmd0512.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class BilanganMax {
    public static void main(String[] args) {
        int a, b, c;
        
        Scanner bilangan = new Scanner(System.in);
        System.out.println("Masukkan Bilangan 1 : ");
        a = bilangan.nextInt();
        System.out.println("Masukkan Bilangan 2 : ");
        b = bilangan.nextInt();
        System.out.println("Masukkan Bilangan 3 : ");
        c = bilangan.nextInt();
        
        if ((a > b) && (a > c)) {
            System.out.println("Bilangan terbesar = " + a);
        } else {
            if ((b > a) && (b > c)) {
                System.out.println("Bilangan terbesar = " + b);
            } else {
                System.out.println("Bilangan terbesar = " + c);
            }
        }
    }
}
