package id.wahyurmd0512.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class HurufVokal {
    public static void main(String[] args) {
        char k;
        Scanner vokal = new Scanner(System.in);
        System.out.println("Masukkan 1 huruf");
        k = vokal.next().charAt(0);
        if (k == 'a' || k == 'i' || k == 'u' || k == 'e' || k == 'o'){
            System.out.println(k + " termasuk dalam huruf vokal");
        }
    }
}
