package id.wahyurmd0512.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class MencetakKata {
    public static void main(String[] args) {
        int angka;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan angka 1 - 4 ");
        angka = in.nextInt();
        
        if (angka == 1) {
            System.out.println("satu");
        } else {
            if (angka == 2) {
                System.out.println("dua");
            } else {
                if (angka == 3) {
                    System.out.println("tiga");
                } else {
                    if (angka == 4) {
                        System.out.println("empat");
                    } else {
                        System.out.println("Angka yang harus dimasukkan harus 1 - 4");
                    }
                }
            }
        }
    }
}
