package id.wahyurmd0512.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class BilanganGenap {

    public static void main(String[] args) {
        int Bilangan;
        
        Scanner bil = new Scanner(System.in);
        System.out.println("Masukkan bilangan bulat : ");
        Bilangan = bil.nextInt();
        
        if (Bilangan % 2 == 0) {
            System.out.println("Bilangan tersebut adalah bilangan genap");
        }
    }
}
