package id.wahyurmd0512.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class BinarySearch {
    public int getBinarySearch(int L[], int n, int x) {
        int j = n - 1, k = 0;
        boolean ketemu;

        int i = 0;
        ketemu = false;
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] < x) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }
    
    public static void main(String[] args) {
        int L[] = {13, 14, 15, 16, 21, 76};
        int n = L.length;

        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nilai yang ingin dicari: ");
        int x = in.nextInt();

        BinarySearch getX = new BinarySearch();
        System.out.println(x + " ada pada indeks: " + getX.getBinarySearch(L, n, x));
    }
}
