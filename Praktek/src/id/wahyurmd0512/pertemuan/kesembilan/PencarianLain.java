package id.wahyurmd0512.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class PencarianLain {

    public int getPencarianLain(int Array[], int n, int x) {
        int i;
        for (i = 0; i < Array.length; i++) {
            if (Array[i] == x) {
                return i;
            } 
        }
        return -1;
    }

    public static void main(String[] args) {
        int Array[] = {13, 16, 14, 21, 76, 15};
        int n = Array.length;

        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nilai yang ingin dicari: ");
        int x = in.nextInt();

        PencarianLain app = new PencarianLain();
        System.out.println("indeks = " + app.getPencarianLain(Array, n, x));
    }

}
