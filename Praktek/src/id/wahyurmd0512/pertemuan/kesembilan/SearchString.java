package id.wahyurmd0512.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class SearchString {

    public int getSearchString(String L[], int n, String x) {
        int i = 0;
        while ((i < n - 1) && (!(L[i].equals(x)))) {
            if (i == 0) {
                System.out.println("Indeks ke-" + i + " hewannya " + L[i]);
            }
            i = i + 1;
            System.out.println("Indeks ke-" + i + " hewannya " + L[i]);
        }

        if (L[i].equals(x)) {
            return i;
        } else {
            return -1;
        }
    }
    
    public int SearchBinaryString(String L[], int n, String x){
        int j = n - 1, k = 0;
        boolean ketemu;

        int i = 0;
        ketemu = false;
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k].equals(x)) {
                ketemu = true;
            } else {
                if (L[k].compareTo(x) < 0) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }
    
    public static void main(String[] args) {
        String L[] = {"ayam", "bebek", "cacing", "sapi"};
        int n = L.length;
        
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan hewan yang ingin dicari: ");
        String x = in.next();
        
        SearchString getString = new SearchString();
        System.out.println("indeks: " + getString.getSearchString(L, n, x));
//        System.out.println("indeks: " + getString.SearchBinaryString(L, n, x));
    }
}
