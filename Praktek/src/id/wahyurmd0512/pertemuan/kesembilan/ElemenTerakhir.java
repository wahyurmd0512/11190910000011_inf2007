package id.wahyurmd0512.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class ElemenTerakhir {
    private int i;
    public int getSearchOutIndeksLast(int L[], int n, int x) {
        i = n - 1;
        while ((i > 0) && (L[i] != x)) {
            if (i == 5) {
                System.out.println("Indeks ke-" + i + " isinya " + L[i]);
            }
            i = i - 1;
            System.out.println("Indeks ke-" + i + " isinya " + L[i]);
        }

        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }
    
    public static void main(String[] args) {
        int L[] = {13, 16, 14, 21, 76, 15};
        int n = L.length;

        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nilai yang ingin dicari: ");
        int x = in.nextInt();

        ElemenTerakhir getX = new ElemenTerakhir();
        System.out.println("indeks: " + getX.getSearchOutIndeksLast(L, n, x));
    }
}
