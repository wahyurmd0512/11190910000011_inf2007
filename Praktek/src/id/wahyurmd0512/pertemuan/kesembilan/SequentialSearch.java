package id.wahyurmd0512.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class SequentialSearch {

    public boolean getSearchOutBoolean(int L[], int n, int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi ke-" + i + " isinya " + L[i]);
        }

        if (L[i] == x) {
            return true;
        } else {
            return false;
        }
    }

    public int getSearchOutIndeks(int L[], int n, int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi ke-" + i + " isinya " + L[i]);
        }

        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }

    public boolean getSearchInBoolean(int L[], int n, int x) {
        int i = 0;
        boolean ketemu = false;
        while ((i < n) && (!ketemu)) {
            if (L[i] == x) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
                ketemu = true;
            } else {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
                i = i + 1;
            }
        }
        return ketemu;
    }

    public int getSearchInIndeks(int L[], int n, int x) {
        int i = 0;
        boolean ketemu = false;

        while ((i < n) && (!ketemu)) {
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;
            }
        }

        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }

    public int getSearchSentinel(int L[], int n, int x) {
        int idx;
        L[n + 1] = x;
        int i = 0;
        while (L[i] != x) {
            i = i + 1;
        }

        if (i == n + 1) {
            return idx = -1;
        } else {
            return idx = i;
        }
    }

    public static void main(String[] args) {
        int L[] = {13, 16, 14, 21, 76, 15};
        int n = L.length;
//        int L[] = new int[7];
//        L[0] = 13;
//        L[1] = 16;
//        L[2] = 14;
//        L[3] = 21;
//        L[4] = 76;
//        L[5] = 15;
//        int n = L.length - 2;

        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nilai yang ingin dicari: ");
        int x = in.nextInt();

        SequentialSearch getX = new SequentialSearch();
        System.out.println("1.a : " + getX.getSearchOutBoolean(L, n, x));
//        System.out.println("1.b : " + getX.getSearchOutIndeks(L, n, x));
//        System.out.println("2.a : " + getX.getSearchInBoolean(L, n, x));
//        System.out.println("2.b : " + getX.getSearchInIndeks(L, n, x));
//        System.out.println("Sentinel indeks: " + getX.getSearchSentinel(L, n, x));
    }
}
