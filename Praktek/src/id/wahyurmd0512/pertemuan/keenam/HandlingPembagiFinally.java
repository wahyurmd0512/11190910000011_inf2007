package id.wahyurmd0512.pertemuan.keenam;

/**
 *
 * @author Wahyu Rmd
 */
public class HandlingPembagiFinally {
    public static void main(String[] args) {
        try {
            int a = 10;
            int b = 0;
            int c = a / b;

            System.out.println("Hasil: " + c);
        } catch (Throwable error) {
            System.out.print("Oops, terjadi error: ");
            System.out.println(error.getMessage());
        } finally {
            System.out.println("Pasti akan dijalankan");
        }
    }
}
