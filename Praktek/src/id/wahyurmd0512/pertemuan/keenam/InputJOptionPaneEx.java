package id.wahyurmd0512.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Wahyu Rmd
 */
public class InputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        System.out.println("Bilangan: " + bilangan);
    }
}
