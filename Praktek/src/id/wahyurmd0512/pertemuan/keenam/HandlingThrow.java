package id.wahyurmd0512.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class HandlingThrow {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("Masukkan angka: ");
            int num = in.nextInt();
            if (num > 10) throw new Exception();
            System.out.println("Angka kurang dari atau sama dengan 10");
        } catch (Exception e) {
            System.out.println("Angka lebih dari 10");
        }
        System.out.println("Selesai");
    }
}
