package id.wahyurmd0512.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Wahyu Rmd
 */
public class OutputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "Bilangan: " + bilangan, "Hasil input", JOptionPane.INFORMATION_MESSAGE);
    }
}
