package id.wahyurmd0512.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class InputScannerEx {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan bilangan: ");
        int bil = in.nextInt();
        
        System.out.println("Bilangan: " + bil);
    }
}
