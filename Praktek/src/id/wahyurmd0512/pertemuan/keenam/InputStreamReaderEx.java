package id.wahyurmd0512.pertemuan.keenam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Wahyu Rmd
 */
public class InputStreamReaderEx {
    public static void main(String[] args) {
        int bilangan;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.print("Masukkan bilangan: ");
        
        try {
            bilangan = Integer.parseInt(in.readLine());
            
            System.out.println("Bilangan: " + bilangan);
        } catch (IOException e) {
            System.out.println("error: " + e.toString());
        }
    }
}
