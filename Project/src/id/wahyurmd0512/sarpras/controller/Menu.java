package id.wahyurmd0512.sarpras.controller;

import id.wahyurmd0512.sarpras.model.Info;
import java.util.Scanner;

/**
 *
 * @author Wahyu Rmd
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Pinjam Barang");
        System.out.println("2. Menu Kembalikan Barang");
        System.out.println("3. Laporan Peminjaman Harian");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");
        
        do {            
            while (!in.hasNextInt()) {                
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }
    
    public void setPilihMenu() {
        SarprasController sc = new SarprasController();
        switch (noMenu) {
            case 1:
                sc.setMeminjamBarang();
                break;
            case 2:
                sc.setKembalikanBarang();
                break;
            case 3:
                sc.getDataSarpras();
                break;
            case 4:
                System.out.println("Sampai Jumpa :)");
                System.exit(0);
                break;
        }
    }
}
