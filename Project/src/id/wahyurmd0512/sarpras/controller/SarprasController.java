package id.wahyurmd0512.sarpras.controller;

import com.google.gson.Gson;
import id.wahyurmd0512.sarpras.model.Sarpras;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Wahyu Rmd
 */
public class SarprasController {

    private static final String FILE = "D:\\temp\\sarpras.json";
    private Sarpras sarpras;
    private final Scanner in;
    private String kodeBarang, kondisiBarang, namaPeminjam;
    private int jenisBarang, NISN;
    private final LocalDateTime waktuMeminjam;
    private LocalDateTime waktuKembalikan;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public SarprasController() {
        in = new Scanner(System.in);
        waktuMeminjam = LocalDateTime.now();
        waktuKembalikan = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setMeminjamBarang() {
        kondisiBarang = "Baik";
        System.out.print("Masukkan nama peminjam: ");
        namaPeminjam = in.nextLine();
        System.out.print("Masukkan NISN: ");
        NISN = in.nextInt();
        System.out.println("Jenis Barang 1=Proyektor, 2=Stop Kontak, 3=Sapu, 4=Lap Pel, 5=pengki");
        System.out.print("Masukkan Jenis Barang : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number");
            System.out.print("Masukkan Jenis Barang : ");
        }
        jenisBarang = in.nextInt();
        System.out.print("Masukkan Kode Barang : ");
        kodeBarang = in.next();
        String formatWaktuMeminjam = waktuMeminjam.format(dateTimeFormat);
        System.out.println("Waktu Meminjam : " + formatWaktuMeminjam);
        System.out.println("Kondisi Barang saat dipinjam: " + kondisiBarang);

        sarpras = new Sarpras();
        sarpras.setNamaPeminjam(namaPeminjam);
        sarpras.setNISN(NISN);
        sarpras.setKodeBarang(kodeBarang.toUpperCase());
        sarpras.setWaktuMeminjam(waktuMeminjam);
        sarpras.setJenisBarang(jenisBarang);
        sarpras.setKondisiBarang(kondisiBarang);

        setWriteSarpras(FILE, sarpras);

        System.out.println("Apakah ingin Meminjam Barang yang Lain ?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMeminjamBarang();
        }
    }

    public void setWriteSarpras(String file, Sarpras sarpras) {
        Gson gson = new Gson();

        List<Sarpras> sarprass = getReadSarpras(file);
        sarprass.remove(sarpras);
        sarprass.add(sarpras);

        String json = gson.toJson(sarprass);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SarprasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setKembalikanBarang() {
        System.out.print("Masukkan Kode Barang : ");
        kodeBarang = in.next();

        Sarpras s = getSearch(kodeBarang);
        if (s != null) {
            LocalDateTime tempWaktu = LocalDateTime.from(s.getWaktuMeminjam());
            waktuKembalikan = LocalDateTime.now();
            long jam = tempWaktu.until(waktuKembalikan, ChronoUnit.HOURS);
            s.setWaktuKembalikan(waktuKembalikan);
            s.setKembalikan(true);

            System.out.println("Nama Peminjam : " + s.getNamaPeminjam());
            System.out.println("NISN : " + s.getNISN());
            System.out.println("Kode Barang : " + s.getKodeBarang());
            System.out.println("Jenis Barang : " + (s.getJenisBarang()));
            System.out.println("Waktu Meminjam : " + s.getWaktuMeminjam());
            System.out.println("Waktu Kembalikan : " + s.getWaktuKembalikan());
            System.out.println("Kondisi Barang saat dipinjam : " + s.getKondisiBarang());
            System.out.println("Kondisi Barang saat di kembalikan ?");
            System.out.print("Baik / Rusak : ");
            kondisiBarang = in.next();

            System.out.println("Kembalikan ?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    setWriteSarpras(FILE, s);
                    break;
                case 2:
                    setKembalikanBarang();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah ingin mengembalikan barang yang lain ?");
            System.out.print("1) Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setKembalikanBarang();
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setKembalikanBarang();
        }
    }

    public Sarpras getSearch(String kodeBarang) {
        List<Sarpras> sarprass = getReadSarpras(FILE);

        Sarpras s = sarprass.stream()
                .filter(ss -> kodeBarang.equalsIgnoreCase(ss.getKodeBarang()))
                .findAny()
                .orElse(null);

        return s;
    }

    public List<Sarpras> getReadSarpras(String file) {
        List<Sarpras> sarprass = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Sarpras[] ps = gson.fromJson(line, Sarpras[].class);
                sarprass.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SarprasController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SarprasController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sarprass;
    }

    public void getDataSarpras() {
        List<Sarpras> sarprass = getReadSarpras(FILE);
        Predicate<Sarpras> isKembalikan = e -> e.isKembalikan() == true;
        Predicate<Sarpras> isDate = e -> e.getWaktuKembalikan().toLocalDate().equals(LocalDate.now());

        List<Sarpras> pResults = sarprass.stream().filter(isKembalikan.and(isDate)).collect(Collectors.toList());
        System.out.println("Nama Peminjam \t\tNISN \t\tKode Barang \tWaktu Meminjam \t\tWaktu Kembali \t\tKondisi Barang");
        System.out.println("--------------- \t--------- \t--------- \t------------------ \t------------------ \t---------");
        pResults.forEach((s) -> {
            System.out.println(s.getNamaPeminjam() + "\t\t" + s.getNISN() + "\t" + s.getKodeBarang() + "\t\t" + s.getWaktuMeminjam().format(dateTimeFormat) + "\t" + s.getWaktuKembalikan().format(dateTimeFormat) + "\t" + s.getKondisiBarang());
        });
        System.out.println("--------------- \t--------- \t--------- \t------------------ \t------------------ \t---------");
        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataSarpras();
        }
    }

}
