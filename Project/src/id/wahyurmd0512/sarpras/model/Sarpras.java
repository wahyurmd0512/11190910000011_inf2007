package id.wahyurmd0512.sarpras.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author Wahyu Rmd
 */
public class Sarpras implements Serializable {

    private static final long serialVersionUID = -6756463875294313469L;
    private String kodeBarang, kondisiBarang, namaPeminjam;
    private int jenisBarang, NISN;
    private LocalDateTime waktuMeminjam;
    private LocalDateTime waktuKembalikan;
    private boolean kembalikan = false;

    public Sarpras() {

    }

    public Sarpras(String kodeBarang, String kondisiBarang, String namaPeminjam, int jenisBarang, int NISN, LocalDateTime waktuMeminjam, LocalDateTime waktuKembalikan) {
        this.kodeBarang = kodeBarang;
        this.kondisiBarang = kondisiBarang;
        this.namaPeminjam = namaPeminjam;
        this.jenisBarang = jenisBarang;
        this.NISN = NISN;
        this.waktuMeminjam = waktuMeminjam;
        this.waktuKembalikan = waktuKembalikan;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getKondisiBarang() {
        return kondisiBarang;
    }

    public void setKondisiBarang(String kondisiBarang) {
        this.kondisiBarang = kondisiBarang;
    }

    public String getNamaPeminjam() {
        return namaPeminjam;
    }

    public void setNamaPeminjam(String namaPeminjam) {
        this.namaPeminjam = namaPeminjam;
    }

    public int getJenisBarang() {
        return jenisBarang;
    }

    public void setJenisBarang(int jenisBarang) {
        this.jenisBarang = jenisBarang;
    }

    public int getNISN() {
        return NISN;
    }

    public void setNISN(int NISN) {
        this.NISN = NISN;
    }

    public LocalDateTime getWaktuMeminjam() {
        return waktuMeminjam;
    }

    public void setWaktuMeminjam(LocalDateTime waktuMeminjam) {
        this.waktuMeminjam = waktuMeminjam;
    }

    public LocalDateTime getWaktuKembalikan() {
        return waktuKembalikan;
    }

    public void setWaktuKembalikan(LocalDateTime waktuKembalikan) {
        this.waktuKembalikan = waktuKembalikan;
    }

    public boolean isKembalikan() {
        return kembalikan;
    }

    public void setKembalikan(boolean kembalikan) {
        this.kembalikan = kembalikan;
    }

    @Override
    public String toString() {
        return "Sarana Prasaranaa{" + "kodeBarang=" + kodeBarang + ", namaBarang=" + jenisBarang + ", waktuMeminjam=" + waktuMeminjam + ", waktuKembalikan=" + waktuKembalikan + ", keluar=" + kembalikan + '}';
    }
}
