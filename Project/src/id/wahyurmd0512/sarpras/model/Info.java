package id.wahyurmd0512.sarpras.model;

/**
 *
 * @author Wahyu Rmd
 */
public class Info {

    private final String aplikasi = "Aplikasi Sarana Prasarana Sederhana";
    private final String version = "Versi 1.0.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}
